#!/usr/bin/env python3
#
# mi_dropbox
# Librería para interactuar con dropbox
#

# CONSTANTES
ACORTADOR = 'http://tinyurl.com/api-create.php?url='

# LIBRERIAS
from dropbox import Dropbox
from dropbox.files import WriteMode
import requests

# FUNCIONES
"""
from io import BytesIO
from random import randint
def drop_toot( nomfich, api ):
    # Sólo mantengo esta función para recordar cómo tratar directamente un fichero sin descargarlo
    # print( 'DIRECTORIO\n', api.files_list_folder( '' ), '\n' )
    nomfich = '/' + nomfich
    metadatos, res = api.files_download( path = nomfich )
    res.raise_for_status()
    with BytesIO( res.content ) as stream:
        urls = stream.readlines()
    res.close()
    numAleatorio = randint( 0, len( urls ) - 1 )
    txt = urls[numAleatorio].strip().decode()
    return txt
"""

class Mi_dropbox():
    def __init__( self, dbx_token ):
        self.dbx = Dropbox( dbx_token )

    def descargar( self, nom_local, nom_dbx ):
        # Descarga el fichero nom_dbx de dropbox a nom_local en local
        try:
            self.dbx.files_download_to_file( nom_local, nom_dbx )
            return True
        except:
            return False

    def cargar( self, nom_local, nom_dbx ):
        # Carga el fichero nom_local de local al fichero nom_dbx en dropbox
        try:
            self.dbx.files_upload( open( nom_local, 'rb' ).read(), nom_dbx, WriteMode( 'overwrite', None ) )
            return True
        except:
            return False

    def _acortar_url( self, url ):
        # Acortar la url dada
        shortUrl = requests.get( ACORTADOR + url )
        return shortUrl.content.decode( 'utf-8' )

    def enlace( self, nom_fich ):
        # Genera un enlace para nom_fich
        url = None
        try:
            url = str( self.dbx.files_get_temporary_link( '/' + nom_fich ) )
            url = url[ url.find( 'link=\'' ) + len( 'link=\'' ) : len( url ) ]
            url = url[ 0 : url.find( '\'' ) ]
            url = url.strip()
            return self._acortar_url( url )
        except:
            return url

