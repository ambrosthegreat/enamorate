# enamorate

___12/12/2020___
Bot para Mastodon que muestra un piropo y una imagen graciosa de una web de dating, ambos al azar. Se puede ver funcionando en https://mastodon.la/web/accounts/22662

En realidad, el código seriviría para publicar cualquier imagen con cualquier texto. Se crea una app en dropbox y en su carpeta se meten las imágenes con un fichero de índice que apunte a todas ellas y se genera un fichero también con las frases.

El bot está pensado para correr desde heroku con el Heroku Scheduler.

El fichero que lanzaríamos es subir_imagen.py
