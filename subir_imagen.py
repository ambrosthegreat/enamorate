#!/usr/bin/env python3
#
# subir_imagen
# Sube una imagen a Mastodon desde una carpeta de Dropbox
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
INST_URL = 'https://botsin.space/'  # URL de la instancia para los bots
LISTADO = 'lista_img.txt'           # Listado de imágenes en la carpeta
MARCA = '[X]'                       # Marca a colocar en el listado para saber qué se ha publicado
PIROPOS = 'piropos.txt'             # Listado de piropos
TEMPORAL = 'temporal.jpg'           # Nombre del fichero descargado (son todos jpg)
TEXTO = 'Quiéreme hasta el infinito y más allá' # Texto a mostrar en cada toot del bot

# LIBRERIAS
from argparse import ArgumentParser
from mi_dropbox import Mi_dropbox
from mi_ficheros import Mi_ficheros
from mi_masto import Mi_masto
from os import environ
from random import randint

# FUNCIONES
def elige_al_azar( mi_lista ):
    # Elige un elemento de la lista al azar
    if len( mi_lista ) > 0:
        numAleatorio = randint( 0, len( mi_lista ) - 1 )
        salida = mi_lista[numAleatorio]
    else:
        salida = ''
    return salida

def salva_marca( fich, cadena ):
    # Añade la MARCA a un determinado elemento del fichero
    lineas = fich.lee_lineas()

    lista = []
    for linea in lineas:
        if linea.strip() != '':
            if linea.strip() != cadena:
                lista.append( linea.strip() )
            else:
                lista.append( MARCA + ' ' + linea.strip() )

    fich.escribe_lista( lista )

def publicar( dbx, masto ):
    # Publicar la imagen en Mastodon
    f_lista = Mi_ficheros( LISTADO )

    if dbx.descargar( f_lista.en_path(), f_lista.en_raiz() ):
        lista_imgs = f_lista.lee_lineas()
        lista_imgs = quita_con_marca( lista_imgs )

        nom_img = '/' + elige_al_azar( lista_imgs )
        f_imagen = Mi_ficheros( TEMPORAL )

        f_piropos = Mi_ficheros( PIROPOS )
        if dbx.descargar( f_piropos.en_path(), f_piropos.en_raiz() ):
            lista_piropos = f_piropos.lee_lineas()
            lista_piropos = quita_con_marca( lista_piropos )

            piropo = elige_al_azar( lista_piropos )
        
            if dbx.descargar( f_imagen.en_path(), nom_img ):
                if masto.toot_foto( piropo,  f_imagen.en_path() ):
                    print( 'publicar() --> Piropo : \"' + piropo + '\"' )
                    print( 'publicar() --> Enviado ' + nom_img )
                    
                    salva_marca( f_lista, nom_img[1:len(nom_img)] )
                    dbx.cargar( f_lista.en_path(), f_lista.en_raiz() )
    
                    f_imagen.borrar()

                    salva_marca( f_piropos, piropo )
                    dbx.cargar( f_piropos.en_path(), f_piropos.en_raiz() )
                else:
                    print( 'publicar() --> No se ha podido enviar ' + nom_img )
            else:
                print( 'publicar() --> Fichero ' + nom_img + ' no encontrado' )
        else:
            print( 'publicar() --> Lista de piropos no encontrada' )
    else:
        print( 'publicar() --> Lista de imágenes no encontrada' )

def quita_con_marca( lista ):
    # Elimina de la lista los elementos que contengan la MARCA
    salida = []
    for elemento in lista:
        if elemento.find( MARCA )<0:
            if elemento.strip() != '':
                salida.append( elemento.strip() )
    return salida

# MAIN
masto = Mi_masto( INST_URL, environ['ENAMORATE_TOKEN'] )
dbx = Mi_dropbox( environ['RUSOS_DBX'] )

publicar( dbx, masto )
