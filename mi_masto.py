#!/usr/bin/env python3
#
# mi_masto
# Librería con mis funciones de acceso a Mastodon
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
MASTODON_MAX = 500              # Máximo número de caracteres admitido
NUM_MENSA = 20                  # Número de notificaciones/mensajes a comprobar por defecto
T_PROCESO = 10                  # Tiempo aproximado que se tarda en procesar una solicitud (en segundos por exceso)

# LIBRERIAS
from mastodon import Mastodon
import os.path as path
import requests
import time
import sys

# FUNCIONES
def _datos_conex( arg_inst_url, arg_app_token ):
    # Leer datos de conexion a Mastodon
    inst_url=''
    app_token=''
    if arg_inst_url:
        inst_url = arg_inst_url
    if arg_app_token:
        app_token = arg_app_token
    if not ( inst_url or app_token ):
        print( 'Faltan los datos de conexión' )
        sys.exit()
    mastodon = Mastodon( access_token = app_token, api_base_url = inst_url )
    print( 'Mi_masto() => Conectado', mastodon.account_verify_credentials()['username'] )
    return mastodon

class Mi_masto():
    def __init__( self, inst_url, app_token, ruta_tmp = '/tmp/' ):
        self.mastodon = _datos_conex( inst_url, app_token )
        self.tmp = ruta_tmp
        if self.tmp[len( self.tmp ) - 1 : len( self.tmp )] != '/':
            self.tmp = self.tmp + '/'

    def toot_foto( self, texto, nom_fich ):
        # Carga un mensaje con imágenes en Mastodon
        mi_jpeg = self.mastodon.media_post( nom_fich, mime_type = 'image/jpeg' )
        lista_media = []
        lista_media.append( mi_jpeg )

        salida = self.mastodon.status_post( texto, media_ids = lista_media )
        return salida
